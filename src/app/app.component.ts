import { Component } from '@angular/core';
import { Plugins } from '@capacitor/core';
import firebase from '@firebase/app';

const { SplashScreen } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor() {
    const firebaseConfig = {
      apiKey: "AIzaSyA_ROFZVzJmAcRsQvI_XpYzOmPRUtDZFbY",
      authDomain: "photo-gallery-7003e.firebaseapp.com",
      databaseURL: "https://capacitortest-c6c6e.firebaseio.com/",
      projectId: "photo-gallery-7003e",
      storageBucket: "photo-gallery-7003e.appspot.com",
      messagingSenderId: "68162899861",
      appId: "1:68162899861:web:df8dd0fe921d5ea7e2f09c"
    };
    // this.initializeApp();
    firebase.initializeApp(firebaseConfig);
  }
}
