import { Photo } from './Photo.models';

export class User {
    constructor(public id: string, public email: string, public password: string, public photo:Photo[]=[]){

    }
}