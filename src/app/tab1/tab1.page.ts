import { Component } from '@angular/core';
import { Map, tileLayer, marker } from 'leaflet';
import { Router, NavigationExtras } from '@angular/router';
import { NativeGeocoder, NativeGeocoderOptions } from "@ionic-native/native-geocoder/ngx";
import { HttpClient } from '@angular/common/http';
import { PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  map: Map;
  newMarker: any;
  address: string[];
  latitude: number;
  longitude: number;

  constructor(private photoService: PhotoService, private router: Router, private geocoder: NativeGeocoder, private http: HttpClient) { }

  ionViewDidEnter() {
    this.loadMap();
  }

  loadMap() {

    this.map = new Map("mapId").setView([43.35, 3.25], 8);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      { attribution: 'Map data © <a href="https://www.openstreetmap.org/"> OpenStreetMap </a>' })
      .addTo(this.map); // This line is added to add the Tile Layer to our map

    this.getAllPic();
  }

  getAllPic() {
    const user = localStorage.getItem('uid');
    return this.http.get('https://capacitortest-c6c6e.firebaseio.com//users/' + user + '/img.json').subscribe(responseData => {
      for (const key in responseData) {
        this.photoService.photos.push(
          responseData[key]
        )
        this.longitude = responseData[key].longitude;
        this.latitude = responseData[key].latitude;
        this.newMarker = marker([this.latitude, this.longitude]).addTo(this.map);
      }
      console.log(this.photoService.photos);
    });
  }

  locatePosition() {
    this.map.locate({ setView: true }).on("locationfound", (e: any) => {
      this.newMarker = marker([e.latitude, e.longitude], {
        draggable:
          true
      }).addTo(this.map);
      this.newMarker.bindPopup("You are located here!").openPopup();
      this.getAddress(e.latitude, e.longitude);

      this.newMarker.on("dragend", () => {
        const position = this.newMarker.getLatLng();
        this.getAddress(position.lat, position.lng);
      });
    });
  }

  getAddress(lat: number, long: number) {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.geocoder.reverseGeocode(lat, long, options).then(results => {
      this.address = Object.values(results[0]).reverse();
    }).catch(err => {
      console.log(err);
    });
  }

  confirmPickupLocation() {
    let navigationextras: NavigationExtras = {
      state: {
        pickupLocation: this.address
      }
    };
    this.router.navigate([""], navigationextras);
  }

  goBack() {
    this.router.navigate([""]);
  }

}
